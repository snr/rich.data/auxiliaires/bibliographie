# Ressources bibliographiques de *Richelieu. Histoire du quartier* et *Rich.Data*

---

## Présentation

Ce dépôt mutualise différentes références bibliographiques dans des fichiers `BibTex`.
Ils sont soit thématiques, soit produits à une fin spécifique (une publication...). Une 
même référence peut être présente dans différents fichiers `bib`.

---

## Licence

Licence ouverte creative commons CC-BY-SA.

